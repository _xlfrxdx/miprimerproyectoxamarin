﻿using Android.OS;
using Proyecto_XDDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoXDDD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddCate : ContentPage
    {
        private ApiRest ApiRest;
        public AddCate(ApiRest api)
        {
            InitializeComponent();
            this.ApiRest = api;
        }
        public async void OnSaveTarea(object sender, EventArgs e)
        {
            var id = 0;
           var response= await ApiRest.Add(txtNombre.Text, txtDescripcion.Text, id);
            if (response)
            {
                await Navigation.PushModalAsync(new MainPage(), false);
            }
            else
            {
                await DisplayAlert("Error", "No se pudo crear la categoria", "Aceptar");
            }
        }
    }
}