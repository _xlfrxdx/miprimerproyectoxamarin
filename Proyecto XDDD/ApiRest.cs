﻿using Android.OS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProyectoXDDD
{
    public class ApiRest
    {
        HttpClient client = new HttpClient();
        string url = "http://192.168.1.68:62023/api/categoria/";

       
        public async Task<List<T>> ListaCategoriat<T>()
        {
           

            client.BaseAddress = new Uri(url);
            try
            {
               
                var response = await client.GetAsync(client.BaseAddress);
                if (response.StatusCode== System.Net.HttpStatusCode.OK)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<T>>(jsonString);
                }
                else
                {
                    string s = "ola";
                    return JsonConvert.DeserializeObject<List<T>>(s);
                }
            }
            catch (Exception )
            {

                throw;
            }
        }
        public async Task<bool> Add(string Nombre, string descripcion, int id )
        {
            if (id <=0)
            {
                client.BaseAddress = new Uri(url);
                Categoria categoria = new Categoria()
                {
                    nombre = Nombre,
                    descripcion = descripcion
                };
                var res = new StringContent(JsonConvert.SerializeObject(categoria), Encoding.UTF8, "application/json");
                var response = await client.PostAsync(client.BaseAddress, res);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                client.BaseAddress = new Uri(url);
                Categoria categoria = new Categoria()
                {
                    id = id,
                    nombre = Nombre,
                    descripcion = descripcion
                };
                var res = new StringContent(JsonConvert.SerializeObject(categoria), Encoding.UTF8, "application/json");
                var response = await client.PutAsync(client.BaseAddress, res);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           
           
        }
        public async Task<bool> Update(string Nombre, string descripcion)
        {

            client.BaseAddress = new Uri(url);
            Categoria categoria = new Categoria()
            {
                nombre = Nombre,
                descripcion = descripcion
            };
            var res = new StringContent(JsonConvert.SerializeObject(categoria), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(client.BaseAddress, res);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public async Task<bool> delate(int id)
        {
            var xdd = url + "Delete/";
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(xdd),
                Content = new StringContent(JsonConvert.SerializeObject(id), Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
            //client.BaseAddress = new Uri(url);
           
        }

    }
}
