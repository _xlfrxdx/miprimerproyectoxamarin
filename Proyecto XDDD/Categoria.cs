﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoXDDD
{
    public class Categoria
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public bool isDeleted { get; set; }
        public DateTime rowVersion { get; set; }
        public object appUser { get; set; }
        public object productos { get; set; }
    }
}
