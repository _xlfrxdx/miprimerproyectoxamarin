﻿using Android.Content;
using ProyectoXDDD;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_XDDD
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        // public  IList<Categoria> categorias { get; private set; }
        private IList<Categoria> categorias = new ObservableCollection<Categoria>();
        public MainPage()
        {

            InitializeComponent();
            Refresh();
            BindingContext = categorias;
            lista.ItemsSource = categorias;
            lista.RefreshCommand = new Command(() => {
                lista.IsRefreshing = true;
                Refresh();
            
            });
         
        }
        async void OnRefresh(object sender, EventArgs e)
        {
            Refresh();
        }

        async private void Refresh()
        {

            ApiRest ola = new ApiRest();
           
            var response = await ola.ListaCategoriat<Categoria>();
            foreach (Categoria categoria in response)
            {
                if (categorias.All(c=> c.id != categoria.id))
                {
                    categorias.Add(categoria);
                }
            }

        }
        async private void OnAddTarea(object sender, EventArgs e)
        {
            ApiRest ola = new ApiRest();
            await Navigation.PushModalAsync(new AddCate(ola));
        }
        async private void Update(object sender, EventArgs e)
        {
            var ola = sender as MenuItem;
            var xd = ola.CommandParameter as Categoria;
            ApiRest api = new ApiRest();
            await Navigation.PushModalAsync(new UpdateCate(api, xd));
           
            //await Navigation.PushModalAsync(new AddCate(ola));
        }
        async private void Eliminar(object sender, EventArgs e)
        {
            var ola = sender as MenuItem;
            var xd = ola.CommandParameter as Categoria;
            ApiRest Api = new ApiRest();

            var response = await Api.delate(xd.id);

            if (response)
            {
                Refresh();
                await DisplayAlert("Success", "La categoria se elimino correctamente", "OK");
                
            }
            else
            {
                Refresh();
                await DisplayAlert("Danger", "La categoria no se pudo eliminar", "OK");
            }

            //await Navigation.PushModalAsync(new AddCate(ola));
        }
        //async private void Remove(object dataSource)
        //{
        //    MenuItem.
        //    var ola = dataSource; 
        //}
        //private void ApiGet_Clicked(object sender, EventArgs e)
        //{
        //    Device.BeginInvokeOnMainThread(async () =>
        //    {



        //    });
        //}
    }
}
