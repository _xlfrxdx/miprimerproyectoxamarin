﻿using Proyecto_XDDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoXDDD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateCate : ContentPage
    {
        private ApiRest ApiRest;
        public UpdateCate(ApiRest api, Categoria categoria)
        {
            BindingContext = categoria;
            InitializeComponent();
            this.ApiRest = api;
        }
        public async void OnSaveTarea(object sender, EventArgs e)
        {
            var parseo = int.Parse(id.Text); 
            var response = await ApiRest.Add(txtNombre.Text, txtDescripcion.Text, parseo);
            if (response)
            {
                await DisplayAlert("success", "se edito correctamente la categoria", "Aceptar");
                await Navigation.PushModalAsync(new MainPage(), false);
            }
            else
            {
                await DisplayAlert("Error", "No se pudo editar la categoria", "Aceptar");
            }
        }
    }
}